# Boilerplate-Parcel

[![License][license-img]](https://github.com/astraloverflow/boilerplate-parcel/blob/master/LICENSE)
[![Version][version-img]](https://github.com/astraloverflow/boilerplate-parcel/releases)
[![Last Commit][last-commit-img]](https://github.com/astraloverflow/boilerplate-parcel/commits/master)
[![Open Issues][issues-img]](https://github.com/astraloverflow/boilerplate-parcel/issues)

> Boilerplate for creating websites with Parcel, the zero-config bundler

---

## Requirements
- Node.js (LTS or newer).
- Latest NPM (comes included with Node.js) or Yarn.

## Quick Start

```shell
$ cd ~/dev/
$ npx degit astraloverflow/boilerplate-parcel#2019-9-27 my-new-website
$ cd my-new-website
$ npm install
$ npm run dev
```

---

## NPM Scripts

### `npm run test`
- Runs [stylelint](https://stylelint.io) (using [stylelint-config-astraloverflow](https://github.com/astraloverflow/stylelint-config-astraloverflow), see `.stylelintrc.json`) and [eslint](https://eslint.org) (using [standardjs](https://standardjs.com) and more, see `.eslintrc.json`) to check files in `src/` for syntax and coding style errors.

### `npm run build`
- Runs Parcel in production mode and builds the project.

### `npm run dev`
- Runs Parcel in development mode and runs a development server.

---

## Package.json

It is HIGHLY recommended that you customize `package.json` with the details of your project, mainly the following fields:

```json5
{
  // Prevents your project from unintentionally being published to NPM.
  "private": true,
  // URL to your project's code repository.
  // If hosted on GitHub you can use the shorthand "user/repo"
  "repository": "",
  // Your project's name, as seen by NPM. See NPM docs for allowed characters.
  "name": "boilerplate-parcel",
  // Your project's description, as seen by NPM. Can be left blank.
  "description": "",
  // SPDX license identifier for your project's license.
  // The list of SPDX license identifiers can be found here: https://spdx.org/licenses/
  "license": "",
  // Your project's version number, as seen by NPM.
  "version": "0.0.0"
}
```

[license-img]: https://img.shields.io/github/license/astraloverflow/boilerplate-parcel.svg
[version-img]: https://img.shields.io/github/release/astraloverflow/boilerplate-parcel.svg
[last-commit-img]: https://img.shields.io/github/last-commit/astraloverflow/boilerplate-parcel.svg
[issues-img]: https://img.shields.io/github/issues-raw/astraloverflow/boilerplate-parcel.svg

